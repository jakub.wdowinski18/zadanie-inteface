﻿using System.Collections.Generic;
using System.Linq;

namespace Zadanie5.kartoteka.impl
{
    public class Kartoteka : IKartoteka
    {
        public readonly List<Osoba> Lista = new List<Osoba>();

        public void dodaj(Osoba objOsoba) => Lista.Add(objOsoba);
        public void usun(Osoba objOsoba) => Lista.Remove(objOsoba);
        public int rozmiar() => Lista.Count();
        public bool czyZawiera(string imie, string nazwisko) => Lista.Exists(item => item.Imie == imie && item.Nazwisko == nazwisko);
        public object pobierz(int index) => Lista.ElementAt(index);
        public Osoba this[int index] { get => Lista.ElementAt(index); }
    }
}
