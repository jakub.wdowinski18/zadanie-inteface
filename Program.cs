﻿using System;
using Zadanie5.kartoteka.impl;
using Zadanie5.kartoteka;
using System.Text;

namespace Zadanie5
{
    class Program
    {
        static void Main(string[] args)
        {
            var kartoteka = new Kartoteka();
            //var osoba = new Osoba("Jan", "Kowalski");
            int opcja = 0;

            Console.WriteLine("1. Dodaj");
            Console.WriteLine("2. Usuń");
            Console.WriteLine("3. Rozmiar");
            Console.WriteLine("4. Czy zawiera");
            Console.WriteLine("5. Pobierz");
            Console.WriteLine("6. Kartoteka");
            Console.WriteLine("7. Wyjdź");

            Console.WriteLine();

            while (opcja != 6)
            {
                Console.Write("Opcja: ");
                int.TryParse(Console.ReadLine().ToString(), out opcja);
                //opcja = Int32.Parse(Console.ReadLine());

                switch (opcja)
                {
                    case 1:
                        {
                            string imie, nazwisko;
                            PobierzDane(out imie, out nazwisko);
                            kartoteka.dodaj(new Osoba(imie, nazwisko));
                            Console.WriteLine();
                            break;
                        }
                    case 2:
                        {
                            string imie, nazwisko;
                            PobierzDane(out imie, out nazwisko);

                            for (int i = kartoteka.rozmiar() - 1; i >= 0; i--)
                            {
                                if ((kartoteka[i] as Osoba).Imie == imie && (kartoteka[i] as Osoba).Nazwisko == nazwisko)
                                    kartoteka.usun(kartoteka[i] as Osoba);
                            }
                            break;
                        }
                    case 3:
                        Console.WriteLine(kartoteka.rozmiar());
                        Console.WriteLine();
                        break;
                    case 4:
                        {
                            string imie, nazwisko;
                            PobierzDane(out imie, out nazwisko);

                            Console.WriteLine(kartoteka.czyZawiera(imie, nazwisko));
                            Console.WriteLine();
                            break;
                        }
                    case 5:
                        Console.Write("Podaj indeks: ");
                        int index;
                        int.TryParse(Console.ReadLine().ToString(), out index);
                        Console.WriteLine($"{(kartoteka.pobierz(index) as Osoba).Imie} {(kartoteka.pobierz(index) as Osoba).Nazwisko}");
                        Console.WriteLine();
                        break;
                    case 6:
                        {
                            var builder = new StringBuilder();
                            for (int i = 0; i < kartoteka.rozmiar(); i++)
                            {
                                builder.AppendLine($"{i} : {kartoteka[i].Imie} {kartoteka[i].Nazwisko}");
                            }
                            Console.WriteLine(builder.ToString());
                            break;
                        }
                    case 7:
                        System.Environment.Exit(0);
                        break;
                    default:
                        break;
                }
            }
        }

        public static void PobierzDane(out string IMIE, out string NAZWISKO)
        {
            Console.Write("Podaj imie: ");
            IMIE = Console.ReadLine().ToString();
            Console.Write("Podaj nazwisko: ");
            NAZWISKO = Console.ReadLine().ToString();

            IMIE ??= "";
            NAZWISKO ??= "";
        }
    }
}
