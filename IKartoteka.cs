﻿using Zadanie5.kartoteka;

namespace Zadanie5
{
    public interface IKartoteka
    {
        public void dodaj(Osoba objOsoba);
        public void usun(Osoba objOsoba);
        public int rozmiar();
        public bool czyZawiera(string imie, string nazwisko);
        public object pobierz(int index);
        public Osoba this[int index] { get; }
    }
}
