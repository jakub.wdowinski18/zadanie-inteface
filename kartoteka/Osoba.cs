﻿namespace Zadanie5.kartoteka
{
    public class Osoba
    {
        public string Imie { get; }
        public string Nazwisko { get; }

        public Osoba(string imie, string nazwisko)
        {
            this.Imie = imie;
            this.Nazwisko = nazwisko;
        }

        //public override bool Equals(object obj) => (obj is Osoba);
    }
}
