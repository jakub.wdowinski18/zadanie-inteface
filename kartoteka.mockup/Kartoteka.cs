﻿using System.Collections.Generic;
using System.Linq;

namespace Zadanie5.kartoteka.mockup
{
    public class Kartoteka : IKartoteka
    {
        public readonly List<Osoba> Lista = new List<Osoba>();

        public void dodaj(Osoba objOsoba) { }
        public void usun(Osoba objOsoba) { }
        public int rozmiar() => 1;
        public bool czyZawiera(string imie, string nazwisko) => true;
        public object pobierz(int index) => new Osoba("Gall", "Anonim");
        public Osoba this[int index] { get => Lista.ElementAt(index); }
    } 
}
